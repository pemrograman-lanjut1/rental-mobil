﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows;

namespace RentalApp.Win10 {
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    
    public partial class App : Application {
        public static MainWindow View { get; set; }
        public static void ViewRouting(bool flag, Control content = null)
        {
            if (flag == true)
            {
                View.GripPanel.Children.Add(content);
            }
            else
            {
                View.GripPanel.Children.Clear();
            }
        }
        private void Application_Startup(object sender, StartupEventArgs e)
        {
            View = new MainWindow();
            View.Show();
        }
    }
}
