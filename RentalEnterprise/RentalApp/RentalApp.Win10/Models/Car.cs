﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace RentalApp.Win10.Models
{
    public class Car
    {
        public int Uid { get; set; }
        public string Merk { get; set; }
        public int Tarif { get; set; }
        public int Mesin { get; set; }
        public int Kursi { get; set; }
        public string Transmision { get; set; }
        public int StatusCar { get; set; }
        public string ImageCar { get; set; } 

    }
}
