﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RentalApp.Win10.Models
{
    public class Transaction
    {
        public DateTime DateTransaksi { get; set; }
        public DateTime DatePinjam { get; set; }
        public DateTime DateKembali { get; set; }
        public int SisaTagihan { get; set; }
        public int DP { get; set; }
        public int Status { get; set; }
        public int Tagihan { get; set; }
        public String Supir { get; set; }
        public Car MobilSewa { get; set; } = new Car();
        public Costomer Penyewa { get; set; } = new Costomer();
    }
}
