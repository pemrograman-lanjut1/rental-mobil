﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RentalApp.Win10.Models
{
    public class Costomer
    {
        public int Uid { get; set; }
        public string Nama { get; set; }
        public DateTime TanggalLahir { get; set; }
        public string Alamat { get; set; }
        public string KTP { get; set; }
        public string SIM { get; set; }
    }
}
