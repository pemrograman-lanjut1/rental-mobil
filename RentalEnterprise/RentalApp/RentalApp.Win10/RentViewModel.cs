﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;
using System.Windows.Input;
using RentalApp.Win10.Models;
using System.Linq;

namespace RentalApp.Win10
{
    public class RentViewModel : BaseViewModel
    {
        public RentViewModel()
        {
            bool cek= false ;
            if (JumlahMobil != 0)
                cek = false;
            if (JumlahMobil == 0)
                cek = true;
            carlist = new ObservableCollection<Car>();
            _selectedCar = new Car();
           ReadCommand = new Command(async () => await ReadCarAsync(cek));
           ReadCommand.Execute(null);
        }

        public ICommand ReadCommand { get; set; }

        public int JumlahMobil
        {
            get
            {
                if (CarList == null)
                    return 0;
                else
                    return CarList.Count();
            }
        }

        public Transaction ModelTransaction
        {
            get
            {
                return _modeltransaction;
            }
            set
            {
                SetProperty(ref _modeltransaction, value);
            }
        }

        public Transaction _modeltransaction;

        public ObservableCollection<Transaction> ListTransaction
        {
            get
            {
                return _listtransaction;
            }
            set
            {
                SetProperty(ref _listtransaction, value);
            }
        }
        private ObservableCollection<Transaction> _listtransaction;

        public ObservableCollection<Car> CarList 
        {
            get 
            { 
                return carlist; 
            }  
            set 
            { 
                SetProperty(ref carlist, value); 
            } 
        }
        public event Action OnReload;

        public Car SelectedCar
        {
            get
            {
                ModelTransaction.MobilSewa = _selectedCar;
                TimeSpan ts = ModelTransaction.DateKembali.Subtract(ModelTransaction.DatePinjam);
                int s = Convert.ToInt32(ts.Days.ToString());
                ModelTransaction.Tagihan = s * _selectedCar.Tarif; 
                return _selectedCar;
            }
            set 
            {
                SetProperty(ref _selectedCar, value);
            }
        }
        private ObservableCollection<Car> carlist;
        private Car _selectedCar;

        private async Task ReadCarAsync(bool asnew = false)
        {
            if (asnew)
            {
                await InitCarAsync();
            }
            else
            {
                OnReload?.Invoke();
            }
        }
        private async Task InitCarAsync()
        {
            await Task.Run(() => {
                CarList = new ObservableCollection<Car> {
                    new Car{Uid =1,Tarif=316200,Mesin=3200,Kursi=6,Transmision="AUTOMATIC",StatusCar=1,Merk="Toyota All New Avanza", ImageCar="https://ik.imagekit.io/tvlk/image/imageResource/2018/05/15/1526377730966-e3c7f7b37d0c97986e64d91350e69110.jpeg"},
                    new Car{Uid =2,Tarif=357000,Mesin=4200,Kursi=4,Transmision="AUTOMATIC",StatusCar=1,Merk="Toyota Calya", ImageCar="https://ik.imagekit.io/tvlk/image/imageResource/2018/09/03/1535973608106-7b8a2d2372ec024ad31279ec333fb9bd.jpeg"},
                    new Car{Uid =3,Tarif=504900,Mesin=3200,Kursi=5,Transmision="AUTOMATIC",StatusCar=1,Merk="Toyota Innova Reborn", ImageCar="https://ik.imagekit.io/tvlk/image/imageResource/2018/05/08/1525764661097-e206ae2b0bc59adb98ad32a1a07b4cbd.jpeg"},
                    new Car{Uid =4,Tarif=357000,Mesin=3200,Kursi=5,Transmision="MANUAL",StatusCar=1,Merk="Daihatsu Xenia", ImageCar="https://ik.imagekit.io/tvlk/image/imageResource/2019/11/18/1574066228855-7d0be2649894c6d3ca5a848635832e4c.jpeg"},
                    new Car{Uid =5,Tarif=357000,Mesin=3200,Kursi=5,Transmision="MANUAL",StatusCar=1,Merk="Daihatsu Sigra", ImageCar="https://ik.imagekit.io/tvlk/image/imageResource/2018/09/03/1535973587223-4cdce7cb0a26e93b8b97be454d4ac0f5.jpeg" }
                };
                ModelTransaction = new Transaction
                {
                    DatePinjam = DateTime.Now,
                    DateKembali = DateTime.Now.AddDays(1),
                };
            });
        }
    }
}
