﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace RentalApp.Win10 {
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window {
        public MainWindow() {
            InitializeComponent();
        }

        private void btn_Home_Click(object sender, RoutedEventArgs e)
        {
            App.ViewRouting(false);
            App.ViewRouting(true, new UserControl1());
            MoveCursorMenu(0);
        }
        private void btn_Rent_Click(object sender, RoutedEventArgs e)
        {
            MoveCursorMenu(1);
            App.ViewRouting(false);
            App.ViewRouting(true, new UserControl_Rent());
        }
        private void btn_History_Click(object sender, RoutedEventArgs e)
        {
            App.ViewRouting(false); ;
            MoveCursorMenu(2);
        }

        private void btn_Transaction_Click(object sender, RoutedEventArgs e)
        {
            App.ViewRouting(false);
            MoveCursorMenu(3);
        }
        private void MoveCursorMenu(int index)
        {
            TrainsitionigContentSlide.OnApplyTemplate();
            GridCursor.Margin = new Thickness(0, (200 + (70 * index)), 0, 0);
        }

    }
}
