﻿using MaterialDesignThemes.Wpf;
using System;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Windows;
using System.Windows.Controls;

namespace RentalApp.Win10
{
    /// <summary>
    /// Interaction logic for UserControl1.xaml
    /// </summary>
    public partial class UserControl1
    {
        public UserControl1()
        {
            
            InitializeComponent();
            tv_tglpinjam.Text = DateTime.Now.ToString("dd/MM/yyyy");
            Calendar.SelectedDate = DateTime.Now;
            var tomorrow = DateTime.Today.AddDays(1);
        }

        private void oke_cl (object sender, RoutedEventArgs e)
        {
                tv_tglpinjam.Text = Calendar.SelectedDate.Value.ToString("dd/MM/yyyy");
        }
    }
    }
