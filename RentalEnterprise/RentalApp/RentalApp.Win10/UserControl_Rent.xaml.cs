﻿using MaterialDesignThemes.Wpf;
using System;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Threading.Tasks;
using System.Collections.ObjectModel;
using System.Collections.Generic;
using RentalApp.Win10.Models;

namespace RentalApp.Win10
{
    /// <summary>
    /// Interaction logic for UserControl_Rent.xaml
    /// </summary>
    public partial class UserControl_Rent : UserControl
    {
        public UserControl_Rent() { 
        
            InitializeComponent();
            if (vm == null)
            {
                vm = new RentViewModel();
            }
            DataContext = vm;
        }
        private RentViewModel vm;

        public async void LstData_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            await Task.Delay(0);
            if (vm.SelectedCar != null)
            {
                GridDetailOrder.Children.Clear();
                GridDetailOrder.Children.Add(new DetailsOrder());
            }
        }

        private void Btn_Cari(object sender, RoutedEventArgs e)
        {
            GridDetailOrder.Children.Clear();
        }


    }
}
